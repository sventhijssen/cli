﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace cli
{
    internal class CreateCliCommand : ICommand
    {
        public CreateCliCommand(List<string> args)
        {
            if (args.Count < 1)
                throw new ArgumentException("Application name is missing.");
            application = args[0];
            guid = Guid.NewGuid();
        }
        public bool Execute()
        {
            Console.WriteLine("Creating application");
            string path = Environment.CurrentDirectory + "\\" + application;
            try
            {
                // Determine whether the directory exists.
                if (Directory.Exists(path))
                {
                    Console.WriteLine("> {0} folder already exists.", application);
                }
                else
                {
                    DirectoryInfo di = Directory.CreateDirectory(path);
                    Console.WriteLine("> {0} folder created", application);

                    Directory.CreateDirectory(Path.Combine(path, "bin"));
                    Console.WriteLine("> bin folder created");

                    Directory.CreateDirectory(Path.Combine(path, "bin\\Debug"));
                    Console.WriteLine("> bin\\Debug folder created");

                    Directory.CreateDirectory(Path.Combine(path, "obj"));
                    Console.WriteLine("> obj folder created");

                    Directory.CreateDirectory(Path.Combine(path, "obj\\Debug"));
                    Console.WriteLine("> obj\\Debug folder created");

                    Directory.CreateDirectory(Path.Combine(path, "Properties"));
                    Console.WriteLine("> Properties folder created");

                    CreateAssemblyInfoFile(path);

                    CreateAppConfigFile(path);

                    CreateCsProjFile(path);

                    CreateProgram(path);

                    CreateClearCommand(path);

                    CreateDocumentationReader(path);

                    CreateExitCommand(path);

                    CreateHelpCommand(path);

                    CreateICommand(path);

                    CreateParser(path);


                }
                Console.WriteLine("Finished");
            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: {0}", e.ToString());
            }
            finally { }
            return false;
        }

        private void CreateAssemblyInfoFile(string path)
        {
            using (FileStream fs = new FileStream(Path.Combine(path, "Properties\\") + "AssemblyInfo.cs", FileMode.Create))
            {
                using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                {
                    w.WriteLine("using System.Reflection;");
                    w.WriteLine("using System.Runtime.CompilerServices;");
                    w.WriteLine("using System.Runtime.InteropServices;");
                    w.WriteLine();
                    w.WriteLine("// General Information about an assembly is controlled through the following");
                    w.WriteLine("// set of attributes. Change these attribute values to modify the information");
                    w.WriteLine("// associated with an assembly.");
                    w.WriteLine("[assembly: AssemblyTitle(\"{0}\")]", application);
                    w.WriteLine("[assembly: AssemblyDescription(\"\")]");
                    w.WriteLine("[assembly: AssemblyConfiguration(\"\")]");
                    w.WriteLine("[assembly: AssemblyCompany(\"\")]");
                    w.WriteLine("[assembly: AssemblyProduct(\"{0}\")]", application);
                    w.WriteLine("[assembly: AssemblyCopyright(\"Copyright ©  {0}\")]", DateTime.Now.Date.Year);
                    w.WriteLine("[assembly: AssemblyTrademark(\"\")]");
                    w.WriteLine("[assembly: AssemblyCulture(\"\")]");
                    w.WriteLine();
                    w.WriteLine("// Setting ComVisible to false makes the types in this assembly not visible");
                    w.WriteLine("// to COM components.  If you need to access a type in this assembly from");
                    w.WriteLine("// COM, set the ComVisible attribute to true on that type.");
                    w.WriteLine("[assembly: ComVisible(false)]");
                    w.WriteLine();
                    w.WriteLine("// The following GUID is for the ID of the typelib if this project is exposed to COM");
                    w.WriteLine("[assembly: Guid(\"{0}\")]", guid.ToString());
                    w.WriteLine();
                    w.WriteLine("// Version information for an assembly consists of the following four values:");
                    w.WriteLine("//");
                    w.WriteLine("//      Major Version");
                    w.WriteLine("//      Minor Version");
                    w.WriteLine("//      Build Number");
                    w.WriteLine("//      Revision");
                    w.WriteLine("//");
                    w.WriteLine("// You can specify all the values or you can default the Build and Revision Numbers");
                    w.WriteLine("// by using the '*' as shown below:");
                    w.WriteLine("// [assembly: AssemblyVersion(\"1.0.* \")]");
                    w.WriteLine("[assembly: AssemblyVersion(\"1.0.0.0\")]");
                    w.WriteLine("[assembly: AssemblyFileVersion(\"1.0.0.0\")]");
                }
            }
            Console.WriteLine("> AssemblyInfo.cs created");
        }

        private void CreateAppConfigFile(string path)
        {
            using (FileStream fs = new FileStream(path + "\\App.config", FileMode.Create))
            {
                using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                {
                    w.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                    w.WriteLine("<configuration>");
                    w.WriteLine("    <startup>");
                    w.WriteLine("        <supportedRuntime version=\"v4.0\" sku=\".NETFramework, Version=v4.6.1\"/>");
                    w.WriteLine("    </startup>");
                    w.WriteLine("</configuration>");
                }
            }
            Console.WriteLine("> App.config created");
        }

        private void CreateCsProjFile(string path)
        {
            using (FileStream fs = new FileStream(path + "\\" + application + ".csproj", FileMode.Create))
            {
                using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                {
                    w.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                    w.WriteLine("<Project ToolsVersion=\"15.0\" xmlns=\"http://schemas.microsoft.com/developer/msbuild/2003\">");
                    w.WriteLine("  <Import Project=\"$(MSBuildExtensionsPath)\\$(MSBuildToolsVersion)\\Microsoft.Common.props\" Condition=\"Exists('$(MSBuildExtensionsPath)\\$(MSBuildToolsVersion)\\Microsoft.Common.props')\" />");
                    w.WriteLine("  <PropertyGroup>");
                    w.WriteLine("    <Configuration Condition=\" '$(Configuration)' == '' \">Debug</Configuration>");
                    w.WriteLine("    <Platform Condition=\" '$(Platform)' == '' \">AnyCPU</Platform>");
                    w.WriteLine("    <ProjectGuid>{0}</ProjectGuid>", guid.ToString());
                    w.WriteLine("    <OutputType>Exe</OutputType>");
                    w.WriteLine("    <RootNamespace>{0}</RootNamespace>", application);
                    w.WriteLine("    <AssemblyName>{0}</AssemblyName>", application);
                    w.WriteLine("    <TargetFrameworkVersion>v4.6.1</TargetFrameworkVersion>");
                    w.WriteLine("    <FileAlignment>512</FileAlignment>");
                    w.WriteLine("    <AutoGenerateBindingRedirects>true</AutoGenerateBindingRedirects>");
                    w.WriteLine("  </PropertyGroup>");
                    w.WriteLine("  <PropertyGroup Condition=\" '$(Configuration)|$(Platform)' == 'Debug|AnyCPU' \">");
                    w.WriteLine("    <PlatformTarget>AnyCPU</PlatformTarget>");
                    w.WriteLine("    <DebugSymbols>true</DebugSymbols>");
                    w.WriteLine("    <DebugType>full</DebugType>");
                    w.WriteLine("    <Optimize>false</Optimize>");
                    w.WriteLine("    <OutputPath>bin\\Debug\\</OutputPath>");
                    w.WriteLine("    <DefineConstants>DEBUG;TRACE</DefineConstants>");
                    w.WriteLine("    <ErrorReport>prompt</ErrorReport>");
                    w.WriteLine("    <WarningLevel>4</WarningLevel>");
                    w.WriteLine("  </PropertyGroup>");
                    w.WriteLine("  <PropertyGroup Condition=\" '$(Configuration)|$(Platform)' == 'Release|AnyCPU' \">");
                    w.WriteLine("    <PlatformTarget>AnyCPU</PlatformTarget>");
                    w.WriteLine("    <DebugType>pdbonly</DebugType>");
                    w.WriteLine("    <Optimize>true</Optimize>");
                    w.WriteLine("    <OutputPath>bin\\Release\\</OutputPath>");
                    w.WriteLine("    <DefineConstants>TRACE</DefineConstants>");
                    w.WriteLine("    <ErrorReport>prompt</ErrorReport>");
                    w.WriteLine("    <WarningLevel>4</WarningLevel>");
                    w.WriteLine("  </PropertyGroup>");
                    w.WriteLine("  <ItemGroup>");
                    w.WriteLine("    <Reference Include=\"System\" />");
                    w.WriteLine("    <Reference Include=\"System.Core\" />");
                    w.WriteLine("    <Reference Include=\"System.Xml.Linq\" />");
                    w.WriteLine("    <Reference Include=\"System.Data.DataSetExtensions\" />");
                    w.WriteLine("    <Reference Include=\"Microsoft.CSharp\" />");
                    w.WriteLine("    <Reference Include=\"System.Data\" />");
                    w.WriteLine("    <Reference Include=\"System.Net.Http\" />");
                    w.WriteLine("    <Reference Include=\"System.Xml\" />");
                    w.WriteLine("  </ItemGroup>");
                    w.WriteLine("  <ItemGroup>");
                    w.WriteLine("    <Compile Include=\"Program.cs\" />");
                    w.WriteLine("    <Compile Include=\"Properties\\AssemblyInfo.cs\" />");
                    w.WriteLine("  </ItemGroup>");
                    w.WriteLine("  <ItemGroup>");
                    w.WriteLine("    <None Include=\"App.config\" />");
                    w.WriteLine("  </ItemGroup>");
                    w.WriteLine("  <Import Project=\"$(MSBuildToolsPath)\\Microsoft.CSharp.targets\" />");
                    w.WriteLine("</Project>");
                }
            }
            Console.WriteLine("> {0}.csproj created", application);
        }

        private void CreateProgram(string path)
        {
            using (FileStream fs = new FileStream(path + "\\Program.cs", FileMode.Create))
            {
                using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                {
                    w.WriteLine("using System;");
                    w.WriteLine("using System.Collections.Generic;");
                    w.WriteLine("using System.Linq;");
                    w.WriteLine("using System.Text;");
                    w.WriteLine("using System.Threading.Tasks;");
                    w.WriteLine("");
                    w.WriteLine("namespace {0}", application);
                    w.WriteLine("{");
                    w.WriteLine("    class Program");
                    w.WriteLine("    {");
                    w.WriteLine("        static void Main(string[] args)");
                    w.WriteLine("        {");
                    w.WriteLine("        }");
                    w.WriteLine("    }");
                    w.WriteLine("}");
                }
            }
            Console.WriteLine("> Program.cs created");
        }

        private void CreateClearCommand(string path)
        {
            using (FileStream fs = new FileStream(path + "\\ClearCommand.cs", FileMode.Create))
            {
                using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                {
                    w.WriteLine("using System;");
                    w.WriteLine("");
                    w.WriteLine("namespace {0}", application);
                    w.WriteLine("{");
                    w.WriteLine("    class ClearCommand : ICommand");
                    w.WriteLine("    {");
                    w.WriteLine("        public bool Execute()");
                    w.WriteLine("        {");
                    w.WriteLine("            Console.Clear();");
                    w.WriteLine("            return false;");
                    w.WriteLine("        }");
                    w.WriteLine("    }");
                    w.WriteLine("}");
                }
            }
            Console.WriteLine("> ClearCommand.cs created");
        }

        private void CreateDocumentationReader(string path)
        {
            using (FileStream fs = new FileStream(path + "\\DocumentatioReader.cs", FileMode.Create))
            {
                using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                {
                    w.WriteLine("using System;");
                    w.WriteLine("using System.Linq;");
                    w.WriteLine("");
                    w.WriteLine("namespace {0}", application);
                    w.WriteLine("{");
                    w.WriteLine("    abstract class DocumentationReader");
                    w.WriteLine("    {");
                    w.WriteLine("        public static void getCommandDocumentation(String command)");
                    w.WriteLine("        {");
                    w.WriteLine("            Command cmd = commands.First(s => s.ShortName == command || s.LongName == command);");
                    w.WriteLine("            Console.WriteLine(\"Usage: \" + cmd.Usage);");
                    w.WriteLine("            Console.WriteLine(cmd.Description);");
                    w.WriteLine("            if(cmd.Flags.Length > 0)");
                    w.WriteLine("                Console.WriteLine(\"Options and arguments\");");
                    w.WriteLine("            foreach(Flag flag in cmd.Flags)");
                    w.WriteLine("            {");
                    w.WriteLine("                Console.Write(\"\".PadRight(2));");
                    w.WriteLine("                Console.Write((flag.ShortName + \" \" + flag.Argument).PadRight(20));");
                    w.WriteLine("                Console.Write(flag.Description);");
                    w.WriteLine("                Console.Write(\"\n\");");
                    w.WriteLine("            }");
                    w.WriteLine("            Console.WriteLine();");
                    w.WriteLine("            Console.WriteLine(\"Mandatory arguments to long options are mandatory for short options too.\");");
                    w.WriteLine("        }");
                    w.WriteLine("");
                    w.WriteLine("        public static void getAllCommandDocumentation()");
                    w.WriteLine("        {");
                    w.WriteLine("            foreach (Command cmd in commands)");
                    w.WriteLine("            {");
                    w.WriteLine("                Console.Write(\"\".PadLeft(2)); // 2chars padding left");
                    w.WriteLine("                Console.Write(cmd.ShortName.PadRight(8)); // 8chars for ShortName + previous = 10chars");
                    w.WriteLine("                Console.Write(cmd.LongName.PadRight(20)); // 12chars for LongName + 10chars previous = 18chars");
                    w.WriteLine("                Console.Write(cmd.Description);");
                    w.WriteLine("                Console.Write(\"\n\");");
                    w.WriteLine("            }");
                    w.WriteLine("        }");
                    w.WriteLine("");
                    w.WriteLine("        private static Command[] commands = {");
                    w.WriteLine("            new Command {");
                    w.WriteLine("                ShortName = \"exit\",");
                    w.WriteLine("                LongName = \"\",");
                    w.WriteLine("                Usage = \"\",");
                    w.WriteLine("                Description = \"Closes this application.\",");
                    w.WriteLine("                Flags = {}");
                    w.WriteLine("            },");
                    w.WriteLine("            new Command {");
                    w.WriteLine("                ShortName = \"help\",");
                    w.WriteLine("                LongName = \"\",");
                    w.WriteLine("                Usage = \"\",");
                    w.WriteLine("                Description = \"\",");
                    w.WriteLine("                Flags = {}");
                    w.WriteLine("            },");
                    w.WriteLine("            new Command");
                    w.WriteLine("            {");
                    w.WriteLine("                ShortName = \"clear\",");
                    w.WriteLine("                LongName = \"\",");
                    w.WriteLine("                Usage = \"clear\",");
                    w.WriteLine("                Description = \"Clears the whole console application.\",");
                    w.WriteLine("                Flags = {}");
                    w.WriteLine("            }");
                    w.WriteLine("        };");
                    w.WriteLine("    }");
                    w.WriteLine("");
                    w.WriteLine("    public class Command");
                    w.WriteLine("    {");
                    w.WriteLine("        public string ShortName { get; set; }");
                    w.WriteLine("        public string LongName { get; set; }");
                    w.WriteLine("        public string Usage { get; set; }");
                    w.WriteLine("        public string Description { get; set; }");
                    w.WriteLine("        public Flag[] Flags;");
                    w.WriteLine("    }");
                    w.WriteLine("");
                    w.WriteLine("    public class Flag");
                    w.WriteLine("    {");
                    w.WriteLine("        public string ShortName { get; set; }");
                    w.WriteLine("        public string LongName { get; set; }");
                    w.WriteLine("        public string Description { get; set; }");
                    w.WriteLine("        public string Argument { get; set; }");
                    w.WriteLine("    }");
                    w.WriteLine("}");
                }
            }
            Console.WriteLine("> DocumentationReader.cs created");
        }

        private void CreateExitCommand(string path)
        {
            using (FileStream fs = new FileStream(path + "\\ExitCommand.cs", FileMode.Create))
            {
                using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                {
                    w.WriteLine("using System;");
                    w.WriteLine("");
                    w.WriteLine("namespace {0}", application);
                    w.WriteLine("{");
                    w.WriteLine("    internal class ExitCommand : ICommand");
                    w.WriteLine("    {");
                    w.WriteLine("        public bool Execute()");
                    w.WriteLine("        {");
                    w.WriteLine("            return true;");
                    w.WriteLine("        }");
                    w.WriteLine("    }");
                    w.WriteLine("}");
                }
            }
            Console.WriteLine("> ExitCommand.cs created");
        }

        private void CreateHelpCommand(string path)
        {
            using (FileStream fs = new FileStream(path + "\\HelpCommand.cs", FileMode.Create))
            {
                using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                {
                    w.WriteLine("using System;");
                    w.WriteLine("using System.Collections.Generic;");
                    w.WriteLine("");
                    w.WriteLine("namespace {0}", application);
                    w.WriteLine("{");
                    w.WriteLine("    internal class HelpCommand : ICommand");
                    w.WriteLine("    {");
                    w.WriteLine("        public HelpCommand(String commandName = \"\")");
                    w.WriteLine("        {");
                    w.WriteLine("            this.commandName = commandName;");
                    w.WriteLine("        }");
                    w.WriteLine("");
                    w.WriteLine("        public bool Execute()");
                    w.WriteLine("        {");
                    w.WriteLine("            if (this.commandName != \"\")");
                    w.WriteLine("            {");
                    w.WriteLine("                DocumentationReader.getCommandDocumentation(this.commandName);");
                    w.WriteLine("                return false;");
                    w.WriteLine("            }");
                    w.WriteLine("            Console.WriteLine(\"For more information on a specific command, type COMMAND \\\"--help\\\"\");");
                    w.WriteLine("            DocumentationReader.getAllCommandDocumentation();");
                    w.WriteLine("            return false;");
                    w.WriteLine("        }");
                    w.WriteLine("");
                    w.WriteLine("        private String commandName = \"\";");
                    w.WriteLine("");
                    w.WriteLine("        private Dictionary<string, string> commands = new Dictionary<string, string>();");
                    w.WriteLine("    }");
                    w.WriteLine("}");
                }
            }
            Console.WriteLine("> HelpCommand.cs created");
        }

        private void CreateICommand(string path)
        {
            using (FileStream fs = new FileStream(path + "\\ICommand.cs", FileMode.Create))
            {
                using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                {
                    w.WriteLine("namespace {0}", application);
                    w.WriteLine("{");
                    w.WriteLine("    public interface ICommand");
                    w.WriteLine("    {");
                    w.WriteLine("       bool Execute();");
                    w.WriteLine("    }");
                    w.WriteLine("}");
                }
            }
            Console.WriteLine("> ICommand.cs created");
        }

        private void CreateParser(string path)
        {
            using (FileStream fs = new FileStream(path + "\\Parser.cs", FileMode.Create))
            {
                using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                {
                    w.WriteLine("using System.Linq;");
                    w.WriteLine("");
                    w.WriteLine("namespace {0}", application);
                    w.WriteLine("{");
                    w.WriteLine("    public static class Parser");
                    w.WriteLine("    {");
                    w.WriteLine("        public static ICommand Parse(string commandString)");
                    w.WriteLine("        {");
                    w.WriteLine("            // Parse your string and create Command object");
                    w.WriteLine("            var commandParts = commandString.Split(' ').ToList();");
                    w.WriteLine("            var commandName = commandParts[0];");
                    w.WriteLine("            var args = commandParts.Skip(1).ToList(); // The arguments list after the command");
                    w.WriteLine("            if (args.Any(str => str.Contains(\"--help\")))");
                    w.WriteLine("                return new HelpCommand(commandName);");
                    w.WriteLine("            switch (commandName)");
                    w.WriteLine("            {");
                    w.WriteLine("                // Create command based on CommandName (and maybe arguments)");
                    w.WriteLine("                case \"exit\": return new ExitCommand();");
                    w.WriteLine("                case \"help\": return new HelpCommand();");
                    w.WriteLine("                case \"clear\": return new ClearCommand();");
                    w.WriteLine("                default:");
                    w.WriteLine("                    return new DefaultCommand();");
                    w.WriteLine("            }");
                    w.WriteLine("        }");
                    w.WriteLine("    }");
                    w.WriteLine("}");
                }
            }
            Console.WriteLine("> Parser.cs created.");
        }

        private void CreateDefaultCommand(string path)
        {
            using (FileStream fs = new FileStream(path + "\\DefaultCommand.cs", FileMode.Create))
            {
                using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                {
                    w.WriteLine("using System;");
                    w.WriteLine("");
                    w.WriteLine("namespace {0}", application);
                    w.WriteLine("{");
                    w.WriteLine("    class DefaultCommand : ICommand");
                    w.WriteLine("    {");
                    w.WriteLine("        public bool Execute()");
                    w.WriteLine("        {");
                    w.WriteLine("            Console.WriteLine(\"Unknown internal command. Type \\\"help\\\" for more information.\");");
                    w.WriteLine("            return false;");
                    w.WriteLine("        }");
                    w.WriteLine("    }");
                    w.WriteLine("}");
                }
            }
            Console.WriteLine("> DefaultCommand created");
        }

        private string application;

        private Guid guid;
    }
}
