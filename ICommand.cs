﻿namespace cli
{
    public interface ICommand
    {
        bool Execute();
    }
}
