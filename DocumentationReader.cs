﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cli
{
    abstract class DocumentationReader
    {
        public static void getCommandDocumentation(String command)
        {
            Command cmd = commands.First(s => s.ShortName == command || s.LongName == command);
            Console.WriteLine("Usage: " + cmd.Usage);
            Console.WriteLine(cmd.Description);
            if (cmd.Flags.Length > 0)
                Console.WriteLine("Options and arguments");
            foreach (Flag flag in cmd.Flags)
            {
                Console.Write("".PadRight(2));
                Console.Write((flag.ShortName + " " + flag.Argument).PadRight(20));
                Console.Write(flag.Description);
                Console.Write("\n");
            }
            Console.WriteLine();
            Console.WriteLine("Mandatory arguments to long options are mandatory for short options too.");
        }

        public static void getAllCommandDocumentation()
        {
            foreach (Command cmd in commands)
            {
                Console.Write("".PadLeft(2)); // 2chars padding left
                Console.Write(cmd.ShortName.PadRight(8)); // 8chars for ShortName + previous = 10chars
                Console.Write(cmd.LongName.PadRight(20)); // 12chars for LongName + 10chars previous = 18chars
                Console.Write(cmd.Description);
                Console.Write("\n");
            }
        }

        private static Command[] commands = {
            new Command {
                ShortName = "new",
                LongName = "",
                Usage = "new APPLICATION_NAME",
                Description = "Creates a command line application with the given name.",
                Flags = { }
            },
            new Command {
                ShortName = "add",
                LongName = "",
                Usage = "add SHORT_COMMAND_NAME [LONG_COMMAND_NAME] DESCRIPTION",
                Description = "Adds a command with the given name.",
                Flags = new Flag[]{
                    new Flag { ShortName = "-a", LongName = "", Argument = "", Description = "indicates whether the command should accept arguments"}
                }
            },
            new Command {
                ShortName = "exit",
                LongName = "",
                Usage = "",
                Description = "Closes this application.",
                Flags = {}
            },
            new Command {
                ShortName = "help",
                LongName = "",
                Usage = "",
                Description = "",
                Flags = {}
            },
            new Command
            {
                ShortName = "clear",
                LongName = "",
                Usage = "clear",
                Description = "Clears the whole console application.",
                Flags = {}
            }
        };
    }

    public class Command
    {
        public string ShortName { get; set; }
        public string LongName { get; set; }
        public string Usage { get; set; }
        public string Description { get; set; }
        public Flag[] Flags;
    }

    public class Flag
    {
        public string ShortName { get; set; }
        public string LongName { get; set; }
        public string Description { get; set; }
        public string Argument { get; set; }
    }
}
